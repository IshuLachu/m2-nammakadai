package com.project.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.dao.CartRepository;
import com.project.dao.OrderRepository;
import com.project.dao.ProductRepository;

import com.project.dao.WishRepository;
import com.project.model.Cart;

import com.project.model.Login;
import com.project.model.MyOrder;
import com.project.model.Product;

import com.project.model.WishList;
import com.project.service.StoreService;

@RestController
@ComponentScan(basePackages = "com.project")
@RequestMapping("/NammaKadai")
public class HomeController {
	@Autowired
	private StoreService service;
	
	@Autowired
    private CartRepository cartservice;

	@Autowired
    private ProductRepository product;
	@Autowired
    private WishRepository wishservice;
	@Autowired
    private OrderRepository orderservice;


private String uName;
	@RequestMapping("/home")
	public ModelAndView home()throws Exception
	{
		return new ModelAndView("HomeStartup");
	}
	@RequestMapping("/homepage")
	public ModelAndView homepage()throws Exception
	{
		return new ModelAndView("HomePage");
	}
	
	
	@RequestMapping("/logout")
	public ModelAndView logout()throws Exception
	{
		return new ModelAndView("HomeStartup");
	}
	@RequestMapping("/thankyou")
	public ModelAndView thankyou(HttpSession session)throws Exception
	{ uName=(String) session.getAttribute("uName");
    session.setAttribute(" uName", uName);
		return new ModelAndView("ThankYou");
	}
	@RequestMapping("/continueShopping")
	public ModelAndView continueShopping()throws Exception
	{
		return new ModelAndView("redirect:/NammaKadai/homepage");
	}
	@RequestMapping("/signup")
	public ModelAndView signup(@ModelAttribute("login") Login login)throws Exception
	{
		return new ModelAndView("SignUp");
	}
	 @RequestMapping("/loginverify")
	public ModelAndView loginVerify(HttpServletRequest request, @ModelAttribute("login") Login login,HttpSession session)throws Exception {
		String uName = request.getParameter("userName");
		String password = request.getParameter("password");
		List<Login> list = service.verifyList();
		String name = "", password1 = "";
		if(uName.equalsIgnoreCase("admin")&&password.equalsIgnoreCase("admin"))
		{
			return new ModelAndView("redirect:/NammaKadai/view");
		}else
		{for(Login l:list){
			name = l.getUserName();
			password1 = l.getPassword();
			if (name.equals(uName) && password1.equals(password)) {
				session.setAttribute("uName",uName);
				return new ModelAndView("redirect:/NammaKadai/homepage");
			}

		}}
		return new ModelAndView("redirect:/NammaKadai/signup");
	}
	 @RequestMapping("/save")
     public ModelAndView save(@ModelAttribute("login") @Valid Login login, BindingResult result)throws IOException {
           if (result.hasErrors()){
        	   return new ModelAndView("Register");  
           }
           else
        	   {service.save(login);
           return new ModelAndView("redirect:/NammaKadai/signup");
           }
	 }
    @RequestMapping("/register")
    public ModelAndView mainhome(@ModelAttribute("login") Login login)throws Exception
	{
		return new ModelAndView("Register");
	}
   

    private List<Product> iterableToCollection(Iterable<Product> iter) {
        List<Product> ls = new ArrayList<Product>();
        for (Product e : iter) {
              ls.add(e);
        }
        return ls;
  }
    
    @RequestMapping(value = "/electronics")
    public ModelAndView paginationElectronics1(HttpSession session)throws Exception {
    Iterable<Product> iter = product.findByCategory("electronics");
    List<Product> list = iterableToCollection(iter);
    
          uName=(String) session.getAttribute("uName");
          session.setAttribute(" uName", uName);
          return new ModelAndView("Electronics", "list", list);
    }
    @RequestMapping(value = "/clothes")
    public ModelAndView cloth(HttpSession session)throws Exception {
    Iterable<Product> iter = product.findByCategory("clothes");
    List<Product> list = iterableToCollection(iter);
    
    uName=(String) session.getAttribute("uName");
          session.setAttribute(" uName", uName);
          return new ModelAndView("Clothes", "list", list);
    }
    @RequestMapping(value = "/toys")
    public ModelAndView toys(HttpSession session)throws Exception {
    Iterable<Product> iter =  product.findByCategory("toys");
    List<Product> list = iterableToCollection(iter);
    
    uName=(String) session.getAttribute("uName");
          session.setAttribute(" uName", uName);
          return new ModelAndView("Toys", "list", list);
    }
    @RequestMapping(value = "/grocery")
    public ModelAndView grocery(HttpSession session)throws Exception {
    Iterable<Product> iter =  product.findByCategory("grocery");
    List<Product> list = iterableToCollection(iter);
    
    uName=(String) session.getAttribute("uName");
          session.setAttribute(" uName",  uName);
          return new ModelAndView("Grocery", "list", list);
    }
    List<Product> cartlist=new ArrayList<Product>();
    @RequestMapping(value = "/cart")
    public ModelAndView update(HttpServletRequest request,HttpSession session)throws Exception {
          int id = Integer.parseInt(request.getParameter("itemId"));
          Iterable<Product> iter = product.findByItemId(id);
          cartlist = iterableToCollection(iter);
          Cart cart=new Cart();
          cart.setItemId(cartlist.get(0).getItemId());
          cart.setImage(cartlist.get(0).getImage());
          cart.setItemName(cartlist.get(0).getItemName());
          cart.setItemBrand(cartlist.get(0).getItemBrand());
          cart.setItemPrice(cartlist.get(0).getItemPrice());
          cart.setDelivery(cartlist.get(0).getDelivery());
          cart.setItemRate(cartlist.get(0).getItemRate());
          cart.setItemDescription(cartlist.get(0).getItemDescription());
          uName=(String) session.getAttribute("uName");
          session.setAttribute("uName", uName);
          cart.setUser(uName);
          cartservice.save(cart);
          return new ModelAndView("redirect:/NammaKadai/saveCart");
    }
    @RequestMapping("/saveCart")
    public ModelAndView saveCart(HttpSession session,HttpServletRequest request)throws Exception {
          
          uName=(String) session.getAttribute("uName");
          session.setAttribute("uName", uName);
          
       
                Iterable<Cart> iter = cartservice.findByUser(uName);
                List<Cart> list = iterableToCollectionCart(iter);
         
          return new ModelAndView("Cart", "list", list);
          }
    private List<Cart> iterableToCollectionCart(Iterable<Cart> iter) {
          List<Cart> ls = new ArrayList<Cart>();
          for (Cart e : iter) {
                ls.add(e);
          }
          return ls;
    }
    @RequestMapping(value = "/wishdelete", method = RequestMethod.GET)
    public ModelAndView wishdelete(HttpServletRequest request) throws Exception{
          int id = Integer.parseInt(request.getParameter("itemId"));
          wishservice.delete(id);
          return new ModelAndView("redirect:/NammaKadai/saveWish");
    }


   
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(HttpServletRequest request) throws Exception{
          int id = Integer.parseInt(request.getParameter("itemId"));
          cartservice.delete(id);
          return new ModelAndView("redirect:/NammaKadai/saveCart");
    }
   
    @RequestMapping("/buy")
    public ModelAndView buy(HttpSession session)throws Exception
    {
          uName=(String) session.getAttribute("uName");
          session.setAttribute("uName", uName);
          List<Login> list1=service.verifyList();
          String address=list1.get(0).getAddress();
          Long sum=cartservice.sumOfPrice();
          session.setAttribute("address", address);
          session.setAttribute("sum", sum);
          return new ModelAndView("redirect:/NammaKadai/order");
    }
    
    @RequestMapping("/wishlist")
    public ModelAndView wishlist(HttpSession session,HttpServletRequest request)throws Exception
    {
          int id = Integer.parseInt(request.getParameter("itemId"));
          Iterable<Product> iter =product.findByItemId(id);
          List<Product> wishlist = iterableToCollection(iter);
          WishList wish=new WishList();
          wish.setItemId(wishlist.get(0).getItemId());
          wish.setImage(wishlist.get(0).getImage());
          wish.setItemName(wishlist.get(0).getItemName());
          wish.setItemBrand(wishlist.get(0).getItemBrand());
          wish.setItemPrice(wishlist.get(0).getItemPrice());
          wish.setDelivery(wishlist.get(0).getDelivery());
          wish.setItemRate(wishlist.get(0).getItemRate());
          wish.setItemDescription(wishlist.get(0).getItemDescription());
          uName=(String) session.getAttribute("uName");
          session.setAttribute("uName", uName);
          wish.setUser(uName);
          wishservice.save(wish);
          return new ModelAndView("redirect:/NammaKadai/saveWish");
          
    }
    @RequestMapping("/saveWish")
    public ModelAndView saveWish(HttpSession session,HttpServletRequest request)throws Exception {
          
          uName=(String) session.getAttribute("uName");
          session.setAttribute("uName", uName);
          System.out.println(uName);
                Iterable<WishList> iter = wishservice.findByUser(uName);
                List<WishList> list = iterableToCollectionWish(iter);
                return new ModelAndView("WishList", "list", list);
    }
    @RequestMapping(value = "/addToCart")
    public ModelAndView addToCart(HttpServletRequest request,HttpSession session) throws Exception{
          int id = Integer.parseInt(request.getParameter("itemId"));
          Iterable<WishList> iter = wishservice.findByItemId(id);
          List<WishList> cartlist=new ArrayList<WishList>();
          cartlist = iterableToCollectionWish(iter);
          Cart cart=new Cart();
          cart.setItemId(cartlist.get(0).getItemId());
          cart.setImage(cartlist.get(0).getImage());
          cart.setItemName(cartlist.get(0).getItemName());
          cart.setItemBrand(cartlist.get(0).getItemBrand());
          cart.setItemPrice(cartlist.get(0).getItemPrice());
          cart.setDelivery(cartlist.get(0).getDelivery());
          cart.setItemRate(cartlist.get(0).getItemRate());
          cart.setItemDescription(cartlist.get(0).getItemDescription());
          uName=(String) session.getAttribute("uName");
          session.setAttribute("uName", uName);
          cart.setUser(uName);
          cartservice.save(cart);
          return new ModelAndView("redirect:/NammaKadai/saveCart");
    }

    private List<WishList> iterableToCollectionWish(Iterable<WishList> iter) {
          List<WishList> ls = new ArrayList<WishList>();
          for (WishList e : iter) {
                ls.add(e);
          }
          return ls;
    }

    										//Pagination
    private Pageable createPageRequest() {
        return new PageRequest(0, 3, Sort.Direction.ASC, "itemId");
    }
@RequestMapping(value = "/clothes/{page}")
    public ModelAndView paginationproduct(@PathVariable int page) throws IllegalAccessException, InvocationTargetException {
                    Slice<Product> slice = null;
          Pageable pageable = createPageRequest();
          List<Product> list=new ArrayList<Product>();
          while (true) {
              slice = product.findByCategory("clothes", pageable);
              int number = slice.getNumber();
              int numberOfElements = slice.getNumberOfElements();
              int size = slice.getSize();
              System.out.printf("slice info - page number %s, numberOfElements: %s, size: %s%n",
                      number, numberOfElements, size);
               list = slice.getContent();
              
               if(number==page){
                      return new ModelAndView("Clothes","list",list);
               }
               
              if (!slice.hasNext()) {
                  break;
              }
              pageable = slice.nextPageable();
          }
      
                    return new ModelAndView("Clothes","list",list);
    }
    
@RequestMapping(value = "/electronics/{page}")
    public ModelAndView paginationproduct1(@PathVariable int page) throws IllegalAccessException, InvocationTargetException {
                    Slice<Product> slice = null;
          Pageable pageable = createPageRequest();
          List<Product> list=new ArrayList<Product>();
          while (true) {
              slice = product.findByCategory("electronics", pageable);
              int number = slice.getNumber();
              int numberOfElements = slice.getNumberOfElements();
              int size = slice.getSize();
              System.out.printf("slice info - page number %s, numberOfElements: %s, size: %s%n",
                      number, numberOfElements, size);
               list = slice.getContent();
              
               if(number==page){
                      return new ModelAndView("Electronics","list",list);
               }
               
              if (!slice.hasNext()) {
                  break;
              }
              pageable = slice.nextPageable();
          }
      
                    return new ModelAndView("Electronics","list",list);
    }
    
@RequestMapping(value = "/grocery/{page}")
    public ModelAndView paginationproduct2(@PathVariable int page) throws IllegalAccessException, InvocationTargetException {
                    Slice<Product> slice = null;
          Pageable pageable = createPageRequest();
          List<Product> list=new ArrayList<Product>();
          while (true) {
              slice = product.findByCategory("grocery", pageable);
              int number = slice.getNumber();
              int numberOfElements = slice.getNumberOfElements();
              int size = slice.getSize();
              System.out.printf("slice info - page number %s, numberOfElements: %s, size: %s%n",
                      number, numberOfElements, size);
               list = slice.getContent();
              
               if(number==page){
                      return new ModelAndView("Grocery","list",list);
               }
               
              if (!slice.hasNext()) {
                  break;
              }
              pageable = slice.nextPageable();
          }
      
                    return new ModelAndView("Grocery","list",list);
    }
    
@RequestMapping(value = "/toys/{page}")
    public ModelAndView paginationproduct3(@PathVariable int page) throws IllegalAccessException, InvocationTargetException {
                    Slice<Product> slice = null;
          Pageable pageable = createPageRequest();
          List<Product> list=new ArrayList<Product>();
          while (true) {
              slice = product.findByCategory("toys", pageable);
              int number = slice.getNumber();
              int numberOfElements = slice.getNumberOfElements();
              int size = slice.getSize();
              System.out.printf("slice info - page number %s, numberOfElements: %s, size: %s%n",
                      number, numberOfElements, size);
               list = slice.getContent();
              
               if(number==page){
                      return new ModelAndView("Toys","list",list);
               }
               
              if (!slice.hasNext()) {
                  break;
              }
              pageable = slice.nextPageable();
          }
      
                    return new ModelAndView("Toys","list",list);
    }
    //order
@RequestMapping("/order")
public ModelAndView order(HttpSession session,HttpServletRequest request) {
	uName=(String) session.getAttribute("uName");
    session.setAttribute("uName", uName);
      Iterable<Cart> iter =cartservice.findAll();
      List<Cart> orderlist=new ArrayList<Cart>();
      orderlist = iterableToCollectionCart(iter);
      MyOrder order=new MyOrder();
      for(int i=0;i<orderlist.size();i++)
      {
      order.setItemId(orderlist.get(i).getItemId());
      order.setImage(orderlist.get(i).getImage());
      order.setItemName(orderlist.get(i).getItemName());
      order.setItemBrand(orderlist.get(i).getItemBrand());
      order.setItemPrice(orderlist.get(i).getItemPrice());
      order.setDelivery(orderlist.get(i).getDelivery());
      order.setItemRate(orderlist.get(i).getItemRate());
      order.setItemDescription(orderlist.get(i).getItemDescription());      
      order.setUser(uName);
      orderservice.save(order);
      }
      cartservice.deleteAll();
      
      return new ModelAndView("redirect:/NammaKadai/saveOrder");
}
@RequestMapping("/saveOrder")
public ModelAndView saveOrder(HttpServletRequest request)throws Exception {
      
     
      return new ModelAndView("Payment");
      }


private List<MyOrder> iterableToCollectionOrder(Iterable<MyOrder> iter) {
	 List<MyOrder> ls = new ArrayList<MyOrder>();
     for (MyOrder e : iter) {
           ls.add(e);
     }
     return ls;
}
@RequestMapping("/myorder")
public ModelAndView myorder(HttpSession session)
{ uName=(String) session.getAttribute("uName");
session.setAttribute("uName", uName);


Iterable<MyOrder> iter =orderservice.findByUser(uName);
List<MyOrder> list = iterableToCollectionOrder(iter);

      return new ModelAndView("Myorder","list",list);
}



    												//ADMIN  
    @RequestMapping("/add")
    public ModelAndView admin(Product product)throws Exception {
          return new ModelAndView("AdminEdit");     
          }


    @RequestMapping(value = "/view") 
    public ModelAndView viewAll() throws Exception{
          List<Product> list = (List<Product>) product.findAll();
          return new ModelAndView("AdminView", "list", list);

    }
    @RequestMapping("/saveproduct")
    public ModelAndView saveproduct(@ModelAttribute("product")Product product1)throws Exception {
          product.save(product1);
          return new ModelAndView("redirect:/NammaKadai/view"); 
          }
    @RequestMapping(value = "deleteAdmin", method = RequestMethod.GET)
    public ModelAndView deleteAdmin(HttpServletRequest request)throws Exception {
          int id = Integer.parseInt(request.getParameter("itemId"));
          product.delete(id);
          return new ModelAndView("redirect:/NammaKadai/view");

    }

    




}
