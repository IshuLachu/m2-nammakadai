package com.project.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


import com.project.model.Cart;


public interface CartRepository extends JpaRepository<Cart,Integer>,PagingAndSortingRepository<Cart,Integer> {
	@Query(value = "SELECT sum(c.item_price) FROM Cart c ", nativeQuery = true)
   Long sumOfPrice();

	Iterable<Cart> findByUser(String user);

}
