package com.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.project.model.MyOrder;

public interface OrderRepository extends JpaRepository<MyOrder,Integer>,PagingAndSortingRepository<MyOrder,Integer> {

	Iterable<MyOrder> findByUser(String uName);

}