package com.project.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


import com.project.model.Product;

public interface ProductRepository extends JpaRepository<Product,Integer>,PagingAndSortingRepository<Product,Integer> {

	Iterable<Product> findByCategory(String category);

	Iterable<Product> findByItemId(int itemId);

	Slice<Product> findByCategory(String string, Pageable pageable);


}
