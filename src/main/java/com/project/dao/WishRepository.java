package com.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.project.model.WishList;

public interface WishRepository extends JpaRepository<WishList,Integer>,PagingAndSortingRepository<WishList,Integer> {

	Iterable<WishList> findByUser(String name);

	Iterable<WishList> findByItemId(int id);

}
