package com.project.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.stereotype.Service;

import com.project.dao.StoreDao;
import com.project.model.Login;


@ComponentScan(basePackages = "com.project")
@Service
public class StoreServiceImpl implements StoreService{
	
@Autowired
private StoreDao dao;

	
	public List<Login> verifyList() {

		
	
		return dao.findAll();
	}


	@Override
	public void save(Login login) {
		dao.save(login);
		
	}
	

		
	}
