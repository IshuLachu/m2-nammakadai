<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/images/icons/Maincart.png"/>
<title>Namma Kadai</title>
<style>
table{
text-align:center;
}
th{
background-color:#80dfff;}
.form{
      width: 32.333%;
      float: center;
      border:3px solid black;
      position: relative;
      height: 300px;
}
.lable{
font-color:red;
}

.logout a:hover
{
background-color: #f2f2f2;
}
.position1 {
      background-color:white ;
      color: black;
      padding: 14px 25px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      opacity: 0.95;
      top: 2%;
      position: absolute;
      left: 90%;
}
</style>

</head>

<body bgcolor="#80dfff">
 <div style="background-color: black">
            <b
                  style="font-family: Bradley Hand ITC; font-size: 30px; color: white">Namma
                  Kadai</b><img src="/images/icons/Maincart.png" width="80" height="80"></img>
            <!--       <marquee direction="right"></marquee> -->
             <a href="/NammaKadai/logout" class="position1 logout btn btn-sign-in">
      <b style="font-family: Bradley Hand ITC; font-size: 20px;">LogOut</b></a>
      </div>
<center>

    <div align="center">
         <div class="container">
         <div class="jumbotron">
        <form:form action="saveproduct" method="post" modelAttribute="product">
       
          <h3>ADD PRODUCT</h3>
        <table>
     
          <form:hidden path="itemId"/>
            <tr>
                <td>Item Name:</td>
                &nbsp;&nbsp;
                <td><form:input path="itemName" /><%-- <label><form:errors path="username"></form:errors></label> --%></td>
            </tr>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <tr>
                <td>Brand:</td>
                &nbsp;&nbsp;
                <td><form:input path="itemBrand" /><%-- <label><form:errors path="password"></form:errors></label> --%></td>
            </tr>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <tr>
                <td>Price:</td>
                &nbsp;&nbsp;
                <td><form:input path="itemPrice" /><%-- <label><form:errors path="password"></form:errors></label> --%></td>
            </tr>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <tr>
                <td>Description:</td>
                &nbsp;&nbsp;
                <td><form:input path="itemDescription" /><%-- <label><form:errors path="password"></form:errors></label> --%></td>
            </tr>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <tr>
                <td>Image:</td>
                &nbsp;&nbsp;
                <td><form:input path="image" /><%-- <label><form:errors path="password"></form:errors></label> --%></td>
            </tr>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <tr><td>Category:</td>
            &nbsp;&nbsp;
    <td>
        <ul>
            <form:select path="category">
                <form:option value="electronics">ELECTRONICS</form:option>
                <form:option value="clothing">CLOTHING</form:option>
                <form:option selected="selected" value="grocery">GROCERY</form:option>
                <form:option value="toys">TOYS</form:option>
            </form:select>
        </ul>
    </td>
    </tr>&nbsp;&nbsp;&nbsp;&nbsp;
            <tr>
                <td colspan="2" align="center"><input type="submit" class="btn btn-success" value="Submit"></td>
            </tr>
        </table>
        </form:form>
        </div>
        </div>
    </div>
    </center>
</body>
</html>