<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" href="/images/icons/Maincart.png"/>
<title>Namma Kadai</title>
<style>
.logout a:hover
{
background-color: #f2f2f2;
}
.position1 {
      background-color:white ;
      color: black;
      padding: 14px 25px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      opacity: 0.95;
      top: 2%;
      position: absolute;
      left: 90%;
}
</style>
</head>
<body>
 <div style="background-color: black">
            <b
                  style="font-family: Bradley Hand ITC; font-size: 30px; color: white">Namma
                  Kadai</b><img src="/images/icons/Maincart.png" width="80" height="80"></img>
            <!--       <marquee direction="right"></marquee> -->
            <a href="/NammaKadai/logout" class="position1 logout btn btn-sign-in">
      <b style="font-family: Bradley Hand ITC; font-size: 20px;">LogOut</b></a>
      </div>
<center>

<div class="container">
<div class="jumbotron">
     <center><h2>PRODUCT DETAILS</h2>&nbsp;<h4 align="right"><a href="add">Add product</a></h4>
     <table border="1" style="border-collapse:collapse" class="table table-striped">
   <tr> <th>ID</th><th>NAME</th><th>BRAND</th><th>PRICE</th><th>DESCRIPTION</th><th>IMAGE</th><th>CATEGORY</th><th>ACTIONS</th></tr>
<core:forEach var="list" items="${list}">
     <tr>
     <td><core:out value="${list.itemId}."></core:out></td>
     <td><core:out value="${list.itemName}"></core:out></td>
       <td><core:out value="${list.itemBrand}"></core:out></td>
        <td><core:out value="${list.itemPrice}"></core:out> </td>
        <td><core:out value="${list.itemDescription}"></core:out> </td>
        <td><core:out value="${list.image}"></core:out> </td>
        <td><core:out value="${list.category}"></core:out> </td>
         <td>
         <a href="deleteAdmin?itemId=${list.itemId}">delete</a> </td>
        
        </core:forEach>
      
         </table>
          
      </center>
</div></div>

</center>
</body>
</html>