<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="icon" href="/images/icons/Maincart.png"/>
<title>Namma Kadai</title>
<style>
.btn {
 background-color: #ffffff;
  color: black;
  padding: 16px 32px;
  text-align: center;
  font-size: 16px;
  margin: 4px 2px;
  transition: 0.3s;
    position: absolute;
    top: 110%;
    left: 84%;
    transform: translate(-50%, -50%);
}

.btn:hover {
 background-color: #000000;
  color: white;

 }
</style>
<script type="text/javascript">
         
            function Redirect() {
               window.location="http://localhost:8111/NammaKadai/homepage";
            }
         
      </script>
</head>
<body>

<div class="w3-container">
    <img src="/images/Thanku/empty-cart.png" class="w3-border" style="padding:16px;width:100%;"> <button class="btn" onclick="Redirect();"><b>Back</b></button>
              
             
</div>
</body>
</html>

