<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="/images/icons/Maincart.png"/>
<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Namma Kadai</title>
<style>

.bg-img1 {
  /* The image used */
  background-image: url("/images/home/Cloth.jpg");
background-size:cover;
  min-height: 250px;
min-width:350px;
  /* Center and scale the image nicely */
  
  /* Needed to position the navbar */
  position: absolute;
      left: 20%;
      top: 20%;
      border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
}
.bg-img4 {
  background-image: url("/images/home/grocery.jpg");
background-size:cover;
  min-height: 250px;
min-width:350px;
  /* Center and scale the image nicely */

  
  /* Needed to position the navbar */
  position: absolute;
      left: 50%;
      top: 60%;
      border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
}
.bg-img2 {
  background-image: url("/images/home/electronics1.jpg");
background-size:cover;
  min-height: 250px;
min-width:350px;
position: absolute;
      left: 50%;
      top: 20%;
      border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
}
.bg-img3 {
  background-image: url("/images/home/Toys1.jpg");
background-size:cover;
  min-height: 250px;
min-width:350px;
position: absolute;
      left: 20%;
      top: 60%;
      border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
}
div:hover {
    box-shadow: 0 0 5px 2px grey;
}

.container {
  position: absolute;
  margin: 20px;
  width: auto;
   top:65%;
  left:2%
}


.topnav {
  overflow: auto;
  background-color: grey;
}


.topnav a {
  color: white;
  font-weight:bold;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-family: Bradley Hand ITC; font-size: 20px;
  position:absolute;
   background-color: black;
}

.topnav a:hover {
  background-color:white ;
  color: black ;
}

.logout a:hover
{
background-color: #f2f2f2;
}
.position1 {
      background-color:white ;
      color: black;
      padding: 14px 25px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      opacity: 0.95;
      top: 2%;
      position: absolute;
      left: 90%;
}
</style>
</head>
<body>
<div style="background-color: black">
                  <b style="font-family: Bradley Hand ITC; font-size: 30px; color: white">Namma Kadai</b>
                  <img src="/images/icons/Maincart.png" width="80" height="80"></img>
                  <span style="position: absolute;top:3%;left:70%;font-family: Bradley Hand ITC; font-size: 20px;color:white">
                  <img src="/images/icons/user1.png" width="50" height="50">
                        <b>${uName} </b>    
                  </span>                  
                  <a href="/NammaKadai/saveWish"><img src="/images/icons/whislist.png" width="50" height="50"/></a>
                  <a href="/NammaKadai/saveCart"><img src="/images/icons/cart.png" width="50" height="50"/></a>
                  <a href="/NammaKadai/myorder"><img src="/images/icons/myorder.png" width="50" height="50"/></a></div>
                  
     

      <a href="/NammaKadai/logout" class="position1 logout btn btn-sign-in">
      <b style="font-family: Bradley Hand ITC; font-size: 20px;">LogOut</b></a>
    

<div class="bg-img1">
  <div class="container">
    <div class="topnav">
      <a href="/NammaKadai/clothes/0">Clothing</a>
    </div>
  </div>
</div>
      
<div class="bg-img2">
  <div class="container">
    <div class="topnav">
      <a href="/NammaKadai/electronics/0">Electronics</a>
    </div>
  </div>
</div>

<div class="bg-img3">
  <div class="container">
    <div class="topnav">
      <a href="/NammaKadai/toys/0">Toys</a>
    </div>
  </div>
</div>

<div class="bg-img4">
  <div class="container">
    <div class="topnav">
      <a href="/NammaKadai/grocery/0">Grocery</a>
    </div>
  </div>
</div>
</body>
</html>

