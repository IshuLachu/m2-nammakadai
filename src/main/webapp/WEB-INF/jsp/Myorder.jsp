<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
          <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="icon" href="/images/header/cart.png"/>
<title>Namma Kadai</title>
<style>
.position1 a:hover
{
 background-color: #f2f2f2;
}
.position2 a:hover
{
 background-color: #f2f2f2;
}
.position3  a:hover
{
 background-color: #f2f2f2;
}
.position1 {
	background-color:white ;
	color: black;
	padding: 14px 25px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	opacity: 0.95;
	top: 2%;
	position: absolute;
	left: 83%;
}
.position2 {
	background-color:white ;
	color: black;
	padding: 14px 25px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	opacity: 0.95;
	top: 2%;
	position: absolute;
	left: 90%;}
	
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Namma Kadai</title>
</head>
<body>
<div style="background-color: black">
                  <b style="font-family: Bradley Hand ITC; font-size: 30px; color: white">Namma Kadai</b>
                  <img src="/images/icons/Maincart.png" width="80" height="80"></img>
                  <b style="font-family: Bradley Hand ITC; font-size: 40px;color:white">MY ORDERS</b>
                  <span style="position: absolute;top:3%;left:65%;font-family: Bradley Hand ITC; font-size: 20px;color:white">
                  <img src="/images/icons/user1.png" width="50" height="50">
                        <b> ${uName} </b>    
                  </span>
                 </div>                

      <a href="/NammaKadai/logout" class="position2 btn btn-sign-in">
      <b style="font-family: Bradley Hand ITC; font-size: 20px;">LogOut</b></a>
      <a href="/NammaKadai/homepage" class="position1 btn btn-sign-in">
      <b style="font-family: Bradley Hand ITC; font-size: 20px;">Home</b></a>
      
      

<div class="container">

<table style="padding:250px 0px 100px 50px">
<c:forEach var="list" items="${list}">
<tr>
<td><img src="/images/products/${list.image}" width="200" height="200"></td>

<td>
<b>Name: </b><c:out value="${list.itemName}"></c:out><br>
<b>Brand: </b><c:out value="${list.itemBrand}"></c:out><br>
<b>Price: </b>Rs.<c:out value="${list.itemPrice}"></c:out><br>
<div class="overlay" >
<img src="/images/icons/${list.itemRate}" width="100" height="30"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<img src="/images/icons/delivery2.png" width="70" height="70"> <b><c:out value="${list.delivery}"></c:out></b>
</div>
</td>
</tr>
</c:forEach>

</table>

</div>

</body>
</html>

